FROM node:8
ARG mode
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
RUN npm install pm2 -g
COPY . .
RUN npm run-script build
EXPOSE 3000
CMD ["pm2-runtime", "start", "npm run start-prod"]
