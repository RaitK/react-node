// Configuration for our NodeJS server
var mode = require('./commons/modes.js');

module.exports = {
  environment: mode.development, // can be set also 'development'
  host: '0.0.0.0',
  port: 3000
}
