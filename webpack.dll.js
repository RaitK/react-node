var path = require("path");
var webpack = require("webpack");
var UglifyJsPlugin = require("uglifyjs-webpack-plugin");

module.exports = {
    mode: 'production',
    entry: {
        vendor: [path.join(__dirname, "client", "vendors.js")]
    },
    performance: {
        hints: false
    },
    output: {
      path: path.join(__dirname, "public", "js"),
      filename: "dll.[name].js",
      library: "[name]"
    },
    plugins: [
        new UglifyJsPlugin(),
        new webpack.DllPlugin({
            path: path.join(__dirname, "client", "[name]-manifest.json"),
            name: "[name]",
            context: path.resolve(__dirname, "client")
        }),
        new webpack.optimize.OccurrenceOrderPlugin()
    ]
};
