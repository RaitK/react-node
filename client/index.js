import React from 'react';
import { render } from 'react-dom';


import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';

import rootReducer from './rootReducer';
import setAuthorizationToken from './utils/setAuthorizationToken';
import jwtDecode from 'jwt-decode';
import { setCurrentUser } from './actions/authActions';
import Routes from './routes';
import styles from '../public/css/bootstrap-v4.css';
import Greetings from './components/Greetings';

// This store is going to remember our component states
const store = createStore(
  rootReducer, // import all reducers inside rootReducer.js
  compose(
    applyMiddleware(ReduxThunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);


// if we have a token in local storage
if (localStorage.jwtToken) {
  setAuthorizationToken(localStorage.jwtToken); // put it in request header
  store.dispatch(setCurrentUser(jwtDecode(localStorage.jwtToken)));
}
console.log("rendering");
// Provier is needed to provide all store for all components
// render(
//    <Greetings/>, document.getElementById('app'));

render(
   <Provider store={store}>
     <Routes/>
  </Provider>, document.getElementById('app'));
