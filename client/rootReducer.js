import { combineReducers } from 'redux';

import auth from './reducers/auth';

// all our needed reducers are initiated in here and combined into
// one object, so we won't need to call them separately

// TODO: add more reducers
export default combineReducers({
  auth
});
