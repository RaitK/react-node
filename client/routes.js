import React from 'react';

import { BrowserRouter, HashRouter, Route, Switch, Link } from 'react-router-dom';

// Import components that we want to route
import App from './components/App';
import Greetings from './components/Greetings';
import SignupPage from './components/signup/SignupPage';
import LoginPage from './components/login/LoginPage';
import NewEventPage from './components/events/NewEventPage';
import requireAuth from './utils/requireAuth';
import PageContentArea from './components/layout/PageContentArea';

import history from './history';

export default () => (

  <BrowserRouter history={history}>
     <div>
      <App/>
      <PageContentArea>
        <Switch>
          <Route exact path='/' component={Greetings}/>
          <Route path='/login' component={LoginPage}/>
          <Route path='/signup' component={SignupPage}/>
          <Route path="/events" component={requireAuth(NewEventPage)}/>
        </Switch>
      </PageContentArea>
     </div>
   </BrowserRouter>
)
