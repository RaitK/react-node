import { SET_CURRENT_USER } from '../actions/types';
import isEmpty from 'lodash/isEmpty';

// initial state, user is not Authenticated
const initialState = {
  isAuthenticated: false,
  user: {}
};

// setup new state
export default (state = initialState, action = {}) => {
  switch(action.type) {
    case SET_CURRENT_USER:
      return {
        isAuthenticated: !isEmpty(action.user),
        user: action.user
      };
    default: return state;
  }
}
