import React from 'react';
import PropTypes from 'prop-types';

class PageContentArea extends React.Component {
  render() {
    return (
      <div className="container">
        {this.props.children}
      </div>
    );
  }
}

export default PageContentArea;
