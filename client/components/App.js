import React from 'react';
import NavigationBar from './NavigationBar';
import { hot } from 'react-hot-loader';

class App extends React.Component {
  render() {
    return (
      <div className="container">
        <NavigationBar />
        {this.props.children}
      </div>
    );
  }
}

export default hot(module)(App);
