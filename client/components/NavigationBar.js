import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logout } from '../actions/authActions';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

class NavigationBar extends React.Component {


  logout(e) {
    e.preventDefault();
    this.props.logout();
  }

  render() {
    const { isAuthenticated } = this.props.auth;

    // only signed in user can see this
    const userLinks = (
      <ul className="nav navbar-nav navbar-right">
        <a href="#" onClick={this.logout.bind(this)}>Logout</a>
      </ul>
    );

    // normal user can see this
    const guestLinks = (
      <ul className="nav navbar-nav navbar-right">
        <Link className="nav-link" to="/signup">Sign up</Link>
        <Link className="nav-link" to="/login">Login</Link>
      </ul>
    );
    console.log("rendering navigation");
    return (
      <nav className="navbar navbar-expand-sm bg-light">
        <div className="container-fluid">
           <a className="navbar-brand">React Skeleton</a>

          <div className="collapse navbar-collapse">
            { isAuthenticated ? userLinks : guestLinks }
          </div>
        </div>
      </nav>
    );
  }
}

NavigationBar.propTypes = {
  auth: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired
}

// function that takes auth state from redux
function mapStateToProps(state) {
  return {
    auth: state.auth
  };
}

// get auth status from redux
export default connect(mapStateToProps, { logout })(NavigationBar);
