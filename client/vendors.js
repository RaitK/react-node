// NOTE: in here we should specify all react major components
// to "DLL" them
require("classnames");
require("lodash");
require("react");
require("react-dom");
require("react-redux");
require("react-hot-loader");
require("react-router-dom");
require("redux");
require("redux-thunk");
require("prop-types");
require("axios");
