import axios from 'axios';

// if we make API calls with axios, then this function will
// add our token to header, if we are not providing token
// then we are going to delete it from header
export default function setAuthorizationToken(token) {
  if (token) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common['Authorization'];
  }
}
