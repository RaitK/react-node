var request = require("request");
var assert = require('chai').assert;
var expect  = require('chai').expect;

var base_url = "http://localhost:3000/";

describe("Server static calls", function() {

  describe("GET /index.html", function() {
    it("returns status code 200", function(done) {

      request.get(base_url, function(error, response, body) {
        assert.isNull(error);
        expect(body).to.have.lengthOf.at.least(10); // Not recommended
        done();
      });

    });
  });

  describe("GET /bundle.js", function() {
    it("returns status code 200", function(done) {

      request.get(base_url + 'bundle.js', function(error, response, body) {
        assert.isNull(error);
        done();
      });

    });
  });


  describe("GET /vendor.js", function() {
    it("returns status code 200", function(done) {

      request.get(base_url + 'vendor.js', function(error, response, body) {
        assert.isNull(error);
        done();
      });

    });
  });


});
