### What is this repository for? ###

This is project skeleton for ReactJS frontend and NodeJS backend development. App itself will be running inside container meaning that it will be same in all servers/computers!

### How do I get set up? (without Docker) ###

Download NodeJS from:
https://nodejs.org/en/download/

From master branch:

```
npm install
npm run-script build
npm start
```

### Docker setup ###

Download Docker from:
https://www.docker.com/

**Development**:

In this mode we are using docker composer and sharing volume with host meaning
that one can have hot-reloaded app inside container

```
docker-compose up --build
```

**Production**:

```
docker build --rm -t react-node:version_1.0 .

docker run --name app -m "300M" --memory-swap "1G" -p 3000:3000 --restart always -d react-node:version_1.0
```

NB! Always remember to specify memory limitations!

**Docker cheatsheet**:

`docker ps -a` See all running containers

`docker rm -f < container name>` Stop container

`docker logs < container name>` See logs for specified container

`docker images` See all the docker images

`docker image rm < image id >` Delete image

### Explore ###

I've added following examples to this project:

* Navigation bar
* Form validation
* Authentication
* Protected pages
* JWT
* API calls
* Clustering
* Unit testing
* Production and Development modes

### Using with PM2 ###

Process manager is a great tool for production environment, which will auto-start Your App on crash.

Just install pm2 and write: "pm2 start npm --start"

PM2 monitoring : pm2 monit


* also added bootstrap v4 for extra fast app launch! :)


### Special thanks ###

https://github.com/Remchi
