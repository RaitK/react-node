import jwt from 'jsonwebtoken';
import config from '../config';

// Authentication
// NOTE: this will be used to verify GET and POST requests
// that need authentication

export default (req, res, next) => {
  // NOTE: remember that we added token header from ReactJS for axios
  const authorizationHeader = req.headers['authorization'];
  let token;

  // the token was added like that `Bearer ${token}`;
  if (authorizationHeader) {
    token = authorizationHeader.split(' ')[1];
  }

  // if we have token
  if (token) {

    // decode token using jwtSecret (key)
    jwt.verify(token, config.jwtSecret, (err, decoded) => {

      // NOTE: Access decoded information like
      // decoded.id etc..

      if (err) { // can't decode token
        res.status(401).json({ error: 'Failed to authenticate' });
        // NOTE: maybe You want to log this?

      } else {

        // we managed to decode token, now it's time to
        // check if the user is valid
        
        // TODO: replace "ok" with Your backend verification logic
        const ok = true;

        if (ok) {
          // TODO: replace 'user_fetched_from_db' from real user
          req.currentUser = 'user_fetched_from_db';
          next();

        } else {

            res.status(404).json({ error: 'No such user' });

        }
      }
    });
  } else { // No token
    res.status(403).json({
      error: 'No token provided'
    });
  }
}
