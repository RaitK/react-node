import express from 'express';
import authenticate from '../middlewares/authenticate';

let router = express.Router();


// NOTE: this post request requires authentication
router.post('/', authenticate, (req, res) => {

  // TODO: add when we failed to auth logic here
  const ok = false;

  if (ok) {
    // IDEA: try to create success message
    res.status(201).json({ success: true });

  }else {
    res.status(400).json({ errors: { form: 'Failed to set!' } });
  }

});

export default router;
