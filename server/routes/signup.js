import express from 'express';

var router = express.Router();

// this post request will signup our user
router.post('/', (req, res) => {

  // TODO: replace ok with your validation logic
  const ok = true;

  if (ok) {

    res.json({ success: true });

  } else {

    // NOTE: 400 is standart HTTP response (Bad Request)
    res.status(400).json({ errors: { form: 'Failed to create account!' } });

  }

});

export default router;
