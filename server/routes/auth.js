
import express from 'express';
import jwt from 'jsonwebtoken';
import config from '../config';

let router = express.Router();

// it takes id and password from posts

router.post('/', (req, res) => {

  // take parameters which we are going to use for auth
  const { identifier, password } = req.body;

  // TODO: add your login logic here -> replace ok with backend validation ->
  // compare identifier, password with your auth logic

  const ok = true;

  if (ok) {
    // create token that user can show
    // NOTE: Please change config.jwtSecret to Your own
    const token = jwt.sign({
      id: '1234', // user ID usually comes from DB
      username: 'user1' // user name can come from DB
    }, config.jwtSecret);

    // respond with token because everything is ok
    res.json({ token });

  } else { // if something went wrong (wrong user, pass etc..)

    // NOTE: 401 is standart HTTP response (Unauthorized )
    res.status(401).json({ errors: { form: 'Failed to sign in!' }});
  }

});

export default router;
