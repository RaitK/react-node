import express from 'express';
import cluster from 'cluster';
import path from 'path';
import bodyParser from 'body-parser';

// configuration for our NodeJS server
import config from '../configuration';
import mode from '../commons/modes';

// NOTE: these import are for development environment and should be removed for production build
import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';

// webpack config is compiled in production mode with webpack itself
var webpackConfig = require('../webpack.config.js');


// TODO: add more API calls
import signup from './routes/signup';
import auth from './routes/auth';
import events from './routes/events';


if (cluster.isMaster && config.environment == mode.production) { // start workers to balance to load for production buiuild

  console.log("Starting production server " + config.environment);

  const cpus = require('os').cpus().length; // total length of CPU's

   for (var i = 0; i < cpus; i++) {
     cluster.fork();
   }

  cluster.on('exit', function(worker) { // auto-restart on crash
    console.log('Restarting worker ' + worker.id);
    cluster.fork();
  });

} else { // development

  let app = express();

  app.use(bodyParser.json());

  // TODO: add different API paths here
  app.use('/api/signup', signup);
  app.use('/api/auth', auth);
  app.use('/api/events', events);

  if (config.environment == mode.development) { // use hot loder for development environment

    // compiler for ReactJS
    const compiler = webpack(webpackConfig);

    // use webpack
    app.use(webpackMiddleware(compiler, {
      hot: true,
      publicPath: webpackConfig.output.publicPath,
      noInfo: true,
      quiet: true,
      stats: {
        assets: false,
        colors: true,
        version: false,
        hash: false,
        timings: true,
        chunks: false,
        chunkModules: false
      }
    }));

    // setup hot reload
    app.use(require("webpack-hot-middleware")(compiler));

  }

  // setup public directory for static files
  app.use('/static', express.static('public', {
    lastModified: false,
    index: false
  }));


  // serve index.html here
  app.get('/*', (req, res) => {
    res.statusCode = 200;
    res.sendFile(path.join(__dirname, '../public/index.html'));
  });

  app.listen(config.port, config.host, () =>
    console.log('Running on ' + config.host + ':' + config.port)
  );

}
