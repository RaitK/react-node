var webpack = require('webpack');
var config = require('./configuration.js');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
var HappyPack = require('happypack');
var path = require('path');
var mode = require('./commons/modes.js');

// common plugins for webpack
var webpackPlugins = [
  new HardSourceWebpackPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.DllReferencePlugin({
         context: path.resolve(__dirname, "client"),
         manifest: require("./client/vendor-manifest.json")
     }),
  new HappyPack({
    loaders: [{
      loader: 'babel-loader',
              query: {
                  cacheDirectory: true
              },
              presets: ["react", "env", "es2015"],
              plugins: ['react-hot-loader/babel']
    }]
  })
];

// webpack plugins for development environment
const webpackDevelopmentPlugins = [
  new webpack.HotModuleReplacementPlugin()
]
// webpack plugins for production environment
const webpackProductionPlugins = [
  new UglifyJsPlugin({
    sourceMap: true
  })
]

// append above plugins
if (config.environment == mode.development) {
  webpackPlugins.push(...webpackDevelopmentPlugins);
} else {
  webpackPlugins.push(...webpackProductionPlugins);
}

module.exports = {
  devtool: (config.environment == mode.development) ? 'eval' : 'source-map',
  devServer: {
    hot: (config.environment == mode.development) ? true : false
  },
  cache: true,
  mode: (config.environment == mode.development) ? 'development' : 'production',
  entry: (config.environment == mode.development) ? [
    path.join(__dirname, '/client/index.js'),
    'webpack-hot-middleware/client?reload=true'] :
    path.join(__dirname, '/client/index.js')
    ,
  output: {
    path: path.resolve(__dirname, 'public/js'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  performance: {
      hints: false
  },
  module: {
    rules: [
      {
        use: 'happypack/loader',
        test: /\.(js|jsx)$/,
        exclude: /node_modules/
      },
      {
        use: ['style-loader', 'css-loader'],
        test: /\.css$/,
        exclude: /node_modules/
      }
    ]
  },
  plugins: webpackPlugins,
  resolve: {
       extensions: [".js", ".jsx"]
   }
};
